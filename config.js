require('dotenv').config();
module.exports = {
  ENV: process.env.NODE_ENV,
  PORT: process.env.PORT,
  URL: process.env.BASE_URL,
  MONGODB_URI: process.env.MONGODB_URI
};
