const Customer = require('../models/Customer');

const customerController = {};

// Get all Customers
customerController.getCustomers = async (req, res, next) => {
  try {
    const customers = await Customer.find();
    customers && customers.length > 0
      ? res.send(customers)
      : res.status(204).send();
    next();
  } catch (e) {
    return next(errorResponse(e, res));
  }
};

// Get single Customer
customerController.getCustomer = async (req, res, next) => {
  try {
    const customer = await Customer.findById(req.params.id);
    customer ? res.send(customer) : res.status(204).send();
    next();
  } catch (e) {
    return next(errorResponse(e, res));
  }
};

// Add Customer
customerController.setCustomer = async (req, res, next) => {
  if (!req.is('application/json')) {
    return next(errorResponse("Expects 'application/json'", res));
  }
  const { name, email, balance } = req.body;
  const customer = new Customer({
    name,
    email,
    balance
  });
  try {
    const newCustomer = await customer.save();
    res.status(201).json(newCustomer);
    next();
  } catch (e) {
    return next(errorResponse(e, res));
  }
};

// Update Customer
customerController.updateCustomer = async (req, res, next) => {
  try {
    const customer = await Customer.findByIdAndUpdate({ _id: req.params.id }, req.body);
    res.status(200).send();
    next();
  } catch (e) {
    return next(errorResponse(e, res));
  }
};

// Delete Customer
customerController.deleteCustomer = async (req, res, next) => {
  try {
    const customer = await Customer.findByIdAndDelete({ _id: req.params.id });
    res.status(204).send();
    next();
  } catch (e) {
    return next(errorResponse(e, res));
  }
};

// Error handler
const errorResponse = (err, response) => {
  console.error(err);
  response.status(500).send(err);
};

module.exports = customerController;
