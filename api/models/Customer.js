const mongoose = require('mongoose');
const timestamp = require('mongoose-timestamp');

const CusatomerSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    trim: true
  },
  email: {
    type: String,
    required: true,
    trim: true
  },
  balance: {
    type: Number,
    default: 0
  }
});

CusatomerSchema.plugin(timestamp);

const Customer = mongoose.model('Customer', CusatomerSchema);
module.exports = Customer;
