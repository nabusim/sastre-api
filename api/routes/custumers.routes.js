const controller = require('../controllers/customer.controller');

module.exports = (server, apiPath) => {
  server.get(apiPath, controller.getCustomers); // All Customers
  server.post(apiPath, controller.setCustomer); // Add Customer
  server.get(`${apiPath}/:id`, controller.getCustomer); // Single Customer
  server.put(`${apiPath}/:id`, controller.updateCustomer); // Update Customer
  server.delete(`${apiPath}/:id`, controller.deleteCustomer); // Delete Customer
};
