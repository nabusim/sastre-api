const express = require('express');
const mongoose = require('mongoose');
const morgan = require('morgan');
const cors = require('cors');
const config = require('./config');

const server = express();

// MiddleWares
server.use(morgan('dev'));
server.use(express.json());
server.use(cors());

// Start Server
server.listen(config.PORT, () => {
  mongoose.connect(
    config.MONGODB_URI,
    { useNewUrlParser: true }
  );
});

const db = mongoose.connection;

db.on('error', err => console.log(err));

// Routes
db.once('open', () => {
  require('./api/routes/index.routes')(server);
  console.log(`Server listening on port ${config.PORT}`);
});
